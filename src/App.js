import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

function Key(props) {
  return (
    <div onClick={props.onClick}>
      {props.value}
    </div>
  )
}

class Calculator extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      input: [],
    }
  }

  renderKey(i) {
    return (
      <Key
        onClick={() => this.handleClick(i)}
        value={i} />
    )
  }

  handleClick(i) {
    var key = this.state.input.slice()
    if (this.state.input.length === 0 && (i === '+' || i === '-' || i === '/' || i === '*' || i === '=')) {
      console.log('enter number')
    } else {
      if (i === '=') {
        this.setState({
          input: [Calculate(this.state.input)],
        })
      } else {
        key.push(i)
        this.setState({
          input: key,
        })
      }
    }

    if (i === 'C') {
      key = []
      this.setState({
        input: key,
      })
    }
  }

  render() {
    const screen = this.state.input.join('')
    return (
      <div class="calculator">
        <div class="input" id="input">{screen}</div>
        <div class="buttons">
          <div class="operators">
            {this.renderKey('+')}
            {this.renderKey('-')}
            {this.renderKey('*')}
            {this.renderKey('/')}
          </div>
          <div class="leftPanel">
            <div class="numbers">
              {this.renderKey(7)}
              {this.renderKey(8)}
              {this.renderKey(9)}
            </div>
            <div class="numbers">
              {this.renderKey(4)}
              {this.renderKey(5)}
              {this.renderKey(6)}
            </div>
            <div class="numbers">
              {this.renderKey(1)}
              {this.renderKey(2)}
              {this.renderKey(3)}
            </div>
            <div class="numbers">
              {this.renderKey(0)}
              {this.renderKey('.')}
              {this.renderKey('C')}
            </div>
          </div>
          <div class="equal" id="result">{this.renderKey('=')}</div>
        </div>
      </div>
    )
  }

}

class App extends Component {
  render() {
    return (
      <div>
        <Calculator />
      </div>
    );
  }
}

function Calculate(input) {
  var value = input.slice().join('')
  return eval(value)
}

export default App;
